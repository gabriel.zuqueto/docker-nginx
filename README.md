# Docker Nginx

Just a example of Nginx static file server with Docker

# Run

Change the NGINX_HOST environment variable in `up.sh` and then execute:: `./up.sh`

To delete the container run: `./down.sh`

## License ##

See [LICENSE](LICENSE).
